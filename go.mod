module gitlab.com/keftcha/diodon

go 1.21

require (
	github.com/bwmarrin/discordgo v0.27.0
	gitlab.com/keftcha/markovchaingo v0.0.0-20231024095121-6052974c3cf6
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
)
